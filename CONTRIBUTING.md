# How to contribute to this macro?

Please take care of the 120 line length limit if reasonable possible and document the code you are adding.

Features and Bugs are here preferred but can of course also be raised in the GSAK Support Forum.
