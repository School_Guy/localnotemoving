# LocalNoteMoving

The Link to the GSAK-Download-Page: [GSAK-Forums](https://gsak.net/board/index.php?showtopic=35195).
The Link to the GSAK-Support-Forum: [GSAK-Forums](https://gsak.net/board/index.php?showtopic=35196).

This macro moves the GSAK DB field d_GcNote to d_UserNote with the scope of a user choosen database.

Learn how you can contribute from the file `CONTRIBUTING.md`.
